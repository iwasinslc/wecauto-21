@extends('admin/layouts.app')

@section('title')
    Редактировать проект
@endsection

@section('breadcrumbs')
    <li><a href="{{route('admin.licences.index')}}">Projects</a></li>
    <li> Редактировать проект "{{ $project->name }}"</li>
@endsection

@section('content')
    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-md-12">

            <!-- tile -->
            <section class="tile">

                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font">Редактировать проект "{{ $project->name }}"</h1>
                    <ul class="controls">
                        <li>
                            <a role="button" tabindex="0" class="tile-fullscreen">
                                <i class="fa fa-expand"></i> {{ __('Fullscreen') }}
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body">

                    <div class="row">
                        <div class="col-lg-6">
                            <form class="form-horizontal" action="{{ route('admin.projects.update', ['id' => $project->id]) }}" enctype="multipart/form-data" method="POST" target="_top" style="width:100%;">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="name" class="col-md-4 control-label">{{ __('Name') }}</label>
                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ $project->name }}" required
                                               autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-md-4 control-label">{{ __('URL') }}</label>
                                    <div class="col-md-6">
                                        <input id="url" type="text" class="form-control" name="url" value="{{ $project->url }}" required
                                               autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-md-4 control-label">{{ __('Description') }}</label>
                                    <div class="col-md-6">
                                <textarea id="description" type="text" class="form-control" name="description" required
                                          autofocus>{{ $project->description }}</textarea>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-primary">
                                            Сохранить проект
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- /tile body -->

            </section>
            <!-- /tile -->

        </div>
        <!-- /col -->
    </div>
    <!-- /row -->


@endsection