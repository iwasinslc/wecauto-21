<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LimitNumberRequests
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle(Request $request, Closure $next)
    {
        switch ($request->method()) {
            case 'POST':
                checkPostLimit();
                break;
        }
        return $next($request);
    }
}
