<?php
namespace App\Http\Controllers\Telegram;

use App\Http\Controllers\Controller;
use App\Models\AutoCreateDeposit;
use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\Rate;
use App\Models\User;
use App\Models\Wallet;

/**
 * Class TelegramTopupController
 * @package App\Http\Controllers\Telegram
 */
class TelegramTopupController extends Controller
{
    /**
     * @param $user_id
     * @param $wallet_id
     * @param $payment_system_id
     * @param $currency_id
     * @param $rate_id
     * @param $amount
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index($user_id, $wallet_id, $payment_system_id, $currency_id, $rate_id, $amount)
    {
        /** @var User $user */
        $user = User::find($user_id);

        if (null == $user) {
            \Log::error('Telegram topup: user is not found');
            abort(503);
        }

        /** @var Wallet $wallet */
        $wallet = Wallet::find($wallet_id);

        if (null == $wallet_id) {
            \Log::error('Telegram topup: wallet is not found');
            abort(503);
        }

        /** @var PaymentSystem $paymentSystem */
        $paymentSystem = PaymentSystem::find($payment_system_id);

        if (null == $paymentSystem) {
            \Log::error('Telegram topup: payment system is not found');
            abort(503);
        }

        /** @var Currency $currency */
        $currency = Currency::find($currency_id);

        if (null == $currency) {
            \Log::error('Telegram topup: currency is not found');
            abort(503);
        }

        /** @var Rate $rate */
        $rate = Rate::find($rate_id);

        if (null == $rate) {
            \Log::error('Telegram topup: rate is not found');
            abort(503);
        }

        if ($rate->currency_id != $currency->id) {
            \Log::error('Telegram topup: rate currency id and currency id is not the same');
            abort(503);
        }

        if ($amount < $rate->min || $amount > $rate->max) {
            \Log::error('Telegram topup: amount is not in range with rate');
            abort(503);
        }

        $searchAutoCreate = AutoCreateDeposit::where('user_id', $user->id)
            ->where('rate_id', $rate->id)
            ->where('wallet_id', $wallet->id)
            ->where('amount', $amount)
            ->where('created_at', '>=', now()->subHour())
            ->first();

        if (null == $searchAutoCreate) {
            \Log::error('Telegram topup: auto create deposit record is not found');
            abort(503);
        }

        try {
            \Auth::login($user);
        } catch (\Exception $e) {
            \Log::error('Telegram topup: can not login - '.$e->getMessage());
            return response('Error: '.$e->getMessage());
        }

        session()->flash('topup.payment_system', $paymentSystem);
        session()->flash('topup.currency', $currency);
        session()->flash('topup.amount', $amount);

        return redirect()->route('profile.topup.' . $paymentSystem->code);
    }
}