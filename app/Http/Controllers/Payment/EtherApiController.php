<?php
namespace App\Http\Controllers\Payment;

use App\Events\NotificationEvent;
use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;
use App\Modules\PaymentSystems\CoinpaymentsModule;
use App\Modules\PaymentSystems\EtherApiModule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class EtherApiController
 * @package App\Http\Controllers\Payment
 */
class EtherApiController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function topUp()
    {
        /** @var PaymentSystem $paymentSystem */
        $paymentSystem = session('topup.payment_system');

        /** @var Currency $currency */
        $currency = session('topup.currency');

        if (($paymentSystem===null) || ($currency===null)) {
            return redirect()->route('profile.topup')->with('error', __('Can not process your request, try again.'));
        }

        $amount = abs(session('topup.amount'));

        $amount = round($amount, 6);
        $user          = Auth::user();

        $wallet = $user->getUserWallet($currency->code, $paymentSystem->code);

        if ($wallet===null) {
            $wallet = Wallet::newWallet($user, $currency, $paymentSystem);
        }

        if (null == $wallet->address) {
            $address = EtherApiModule::getAddress($currency->code);
            $wallet->address = $address;
            if ($currency->code!='ETH')
            {
                $wallet->address = $address->address;
            }

            $wallet->save();
            $wallet->fresh();
        }

        return view('ps.etherapi', [
            'currency' => $currency->code,
            'amount' => $amount,
            'statusUrl' => route('perfectmoney.status'),
            'user' => $user,
            'wallet' => $wallet,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function status(Request $request)
    {



        \Log::alert('status--'.print_r($request->all(),true));

        $rawRequest = json_decode(file_get_contents('php://input'), true);

        if ($rawRequest === FALSE || empty($rawRequest)|| !isset($rawRequest['etherapi.net'])) {
            \Log::critical('EtherApi. Strange request from: '.$request->ip().', Error reading POST data. '.print_r($request->all(),true));
            return response('ok');
        }



        $sign = sha1(implode(':', array(
            $rawRequest['type'],
            $rawRequest['date'],
            $rawRequest['from'],
            $rawRequest['to'],
            $rawRequest['amount'],
            $rawRequest['txid'],
            $rawRequest['confirmations'],
            $rawRequest['tag'],
            env('ETHER_API_KEY') // токен доступа к API
        )));



        if (isset($rawRequest['token']))
        {
            $sign = sha1(implode(':', array(
                $rawRequest['type'],
                $rawRequest['date'],
                $rawRequest['from'],
                $rawRequest['to'],
                $rawRequest['token'],
                $rawRequest['amount'],
                $rawRequest['txid'],
                $rawRequest['confirmations'],
                $rawRequest['tag'],
                env('ETHER_API_KEY') // токен доступа к API
            )));
        }

        if ($sign !== $rawRequest['sign']) {
            \Log::critical('EtherApi. Strange request from: '.$request->ip().', HMAC signature does not match. '.print_r($request->all(),true));
            return response('ok');
        }

        if (!$request->has('amount') ||

            !$request->has('txid') ||
            !$request->has('type') ||
            $request->type!='in' ||
            !$request->has('tag')) {
            \Log::info('EtherApi. Strange request from: '.$request->ip().'. Entire request is: '.print_r($request->all(),true));
            return response('ok');
        }

        /** @var PaymentSystem $paymentSystem */
        $paymentSystem = PaymentSystem::where('code', 'etherapi')->first();
        /** @var Currency $currency */


        $currency      = Currency::where('code', strtoupper("ETH"))->first();


        if (isset($rawRequest['token']))
        {
            $currency      = Currency::where('code', strtoupper($rawRequest['token']))->first();
        }

        if (null == $currency) {
            \Log::critical('Strange request from: '.$request->ip().'. Currency not found. Entire request is: '.print_r($request->all(),true));
            return response('ok');
        }


        /** @var Wallet $wallet */
        $wallet = Wallet::where('currency_id', $currency->id)
            ->where('payment_system_id', $paymentSystem->id)
            ->where('address', $request->to)
            ->first();

        if ($wallet===null)
        {
            \Log::critical('Strange request from: '.$request->ip().'. User not found. Entire request is: '.print_r($request->all(),true));
        }


        /** @var User $user */
        $user = $wallet->user;




        /** @var Transaction $transaction */

        $transaction = Transaction::where('batch_id', $request->txid)->first();
        if ($request->confirmations >= 2&&$transaction===null)
        {
            $transaction = Transaction::enter($wallet, $request->amount);

            if (null!==$transaction)
            {

                $transaction->batch_id = $request->txid;
                $transaction->result = 'complete';
                $transaction->source = $request->to;
                $transaction->save();
                $commission = $transaction->amount * 0.01 * $transaction->commission;

                //$wallet->refill(($transaction->amount-$commission), $transaction->source);
                $transaction->update(['approved' => true]);




                $amount = ($transaction->amount - $commission)*rate($currency->code, 'USD');

                $wallet = $user->getUserWallet('USD', 'perfectmoney');


                $exchange = Transaction::enter_exchange($wallet, $amount, Currency::getByCode('USD'), PaymentSystem::getByCode('perfectmoney'));

                /** @var Wallet $wallet */


                $wallet->refill($amount);

                NotificationEvent::dispatch($user, 'notifications.wallet_refiled', [
                    //'id'=>$user->id,
                    'user_id'=>$user->id,
                    'amount'=>$exchange->amount,
                    'currency'=>$exchange->currency->code
                ]);

                EtherApiModule::getBalances(); // обновляем баланс нашего внешнего кошелька в БД
                return response('OK');

            }
        }






        \Log::emergency('EtherApi transaction is not passed. IP: '.$request->ip().'. '.print_r($request->all(), true));
        return response('OK');
    }
}
