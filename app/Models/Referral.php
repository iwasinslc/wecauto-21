<?php
namespace App\Models;

use App\Traits\ModelTrait;
use App\Traits\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Referral
 * @package App\Models
 *
 * @property integer level
 * @property float percent
 * @property float percent_representative
 * @property integer on_load
 * @property integer on_profit
 * @property integer on_task
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class Referral extends Model
{
    use Uuids;
    use ModelTrait;

    /** @var bool $incrementing */
    public $incrementing = false;
    protected $keyType = 'string';

    /** @var array $fillable */
    protected $fillable = [
        'level',
        'percent',
        'percent_representative',
        'on_load',
        'on_profit',
        'on_task',
    ];

    /**
     * @param $level
     * @return int
     */
    public static function getOnLoad($level)
    {
        if ($referral = self::where('level', $level)->first()) {
            if ($referral->on_load) return $referral->percent;
            return 0;
        }
        return 0;

    }

    /**
     * @param $level
     * @return int
     */
    public static function getOnProfit($level)
    {
        if ($referral = self::where('level', $level)->first()) {
            if ($referral->on_profit) return $referral->percent;
            return 0;
        }
        return 0;
    }

    /**
     * @param $level
     * @return int
     */
    public static function getOnTask($level)
    {
        if ($referral = self::where('level', $level)->first()) {
            if ($referral->on_task) return $referral->percent;
            return 0;
        }
        return 0;
    }

    /**
     * @param $level
     * @return int
     */
    public static function getOnLoadRepresentative($level)
    {
        if ($referral = self::where('level', $level)->first()) {
            if ($referral->on_load) return $referral->percent_representative;
            return 0;
        }
        return 0;

    }

    /**
     * @param $level
     * @return int
     */
    public static function getOnProfitRepresentative($level)
    {
        if ($referral = self::where('level', $level)->first()) {
            if ($referral->on_profit) return $referral->percent_representative;
            return 0;
        }
        return 0;
    }

    /**
     * @param $level
     * @return int
     */
    public static function getOnTaskRepresentative($level)
    {
        if ($referral = self::where('level', $level)->first()) {
            if ($referral->on_task) return $referral->percent_representative;
            return 0;
        }
        return 0;
    }
}
