<?php

namespace App\Models;

use App\Events\ExchangeAllOperations;
use App\Events\ExchangeUserOperations;
use App\Events\NotificationEvent;
use App\Events\OrderCreated;
use App\Events\OrderUpdated;
use App\Events\TradeConfirm;
use App\Traits\ModelTrait;
use App\Traits\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class ExchangeOrder
 * @package App\Models
 *
 * @property integer id
 * @property string currency_id
 * @property string main_currency_id
 * @property string wallet_id
 * @property string main_wallet_id
 * @property string type
 * @property string user_id
 * @property float amount
 * @property User user
 * @property Wallet mainWallet
 * @property Wallet wallet
 * @property float wallet_balance
 * @property integer active
 * @property float invest_amount
 * @property integer closed
 * @property integer need_pay
 * @property float rate
 * @property float licence_rate
 * @property float remove_amount
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class ExchangeOrder extends Model
{
    use ModelTrait;

    const TYPE_BUY = 1;
    const TYPE_SELL = 0;

    /** @var array $fillable */
    protected $fillable = [
        'currency_id',
        'main_currency_id',
        'wallet_id',
        'main_wallet_id',
        'type',
        'user_id',
        'amount',
        'closed',
        'active',
        'invest_amount',
        'rate',
        'remove_amount',
        'licence_rate'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wallet()
    {
        return $this->belongsTo(Wallet::class, 'wallet_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mainCurrency()
    {
        return $this->belongsTo(Currency::class, 'main_currency_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mainWallet()
    {
        return $this->belongsTo(Wallet::class, 'main_wallet_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'order_id');
    }


    /**
     * @param $field
     * @return ExchangeOrder|null
     * @throws \Throwable
     */


    public static function addOrder($field)
    {
        $amount = $field['amount'];

        /** @var User $user */
        $user = isset($field['user']) ? $field['user'] : user();
        /** @var Wallet $wallet */


        $wallet = $user->wallets()->where('id', $field['main_wallet_id'])->first();

        $rate = isset($field['rate']) ? $field['rate'] : rate($wallet->currency->code, 'USD');


        $type = isset($field['type']) ? $field['type'] : 1;


        /** @var Wallet $second_wallet */
        $second_wallet = $user->wallets()->find($field['wallet_id']);


//        try {


        $order = new ExchangeOrder();
        $order->user_id = $user->id;
        $order->main_currency_id = $wallet->currency_id;
        $order->currency_id = $second_wallet->currency_id;
        $order->main_wallet_id = $wallet->id;
        $order->wallet_id = $second_wallet->id;
        $order->type = $type;
        $order->amount = $amount;
        $order->rate = $rate;
        $order->licence_rate = $rate * rate($second_wallet->currency->code, 'USD');


        $order->save();


        \Log::error('add order, remove amount from wallet ' . $wallet->id . ', amount ' . $amount);


        //if (null != $transaction) {


        try {




            $order->update(['active' => 1]);
        } catch (\Exception $e) {

            throw new \Exception($e->getMessage());
        }


        NotificationEvent::dispatch($order->user, 'notifications.order_created', [
            'id' => $order->id,
            'user_id' => $order->user_id,
            'amount' => $order->amount,
            'currency' => $order->mainCurrency->code
        ]);

        //ExchangeUserOperations::dispatch($order->user);
        //event(new ExchangeAllOperations($amount));
        return $order;
        //};
    }


    public function close()
    {
        try {


            DB::transaction(function () {

                /**
                 * @var Wallet $mainWallet
                 */

                $this->active = 0;
                $this->closed = 1;

                /**
                 * @var User $user
                 */
                $user = $this->user;

                $mainWallet = $this->type==self::TYPE_SELL?  $this->mainWallet()->lockForUpdate()->first() : $this->wallet()->lockForUpdate()->first();

                if (null===$mainWallet) return false;


                $amount = $this->type == self::TYPE_SELL ? $this->amount : $this->amount * $this->rate;

                $this->save();


                if ($this->mainWallet->currency->code == 'FST') {

                    if ($user->activeLicence()) {
                        $parse = $user->buy_at;

                        if ($this->created_at > $parse->toDateTimeString()) {
                            if ($this->type == ExchangeOrder::TYPE_SELL) {
                                //PriceUps::priceCloseUp($amount);
                                $user->addLimitsSell($this->amount, $this->wallet->currency->code, $this->licence_rate);
                            } else {
                                $user->addLimitsBuy($this->amount, $this->wallet->currency->code, $this->licence_rate);
                            }
                        }
                    }
                }

                elseif($this->mainWallet->currency->code == 'WEC') {
                    if ($this->type == ExchangeOrder::TYPE_SELL) {
                        if ($this->created_at>='2021-04-15 15:00:00')
                        //PriceUps::priceDown($amount);
                        $user->addDepositSellLimit($this->amount * $this->rate);

                    }
                }


                $transaction = Transaction::exchange_close_order($mainWallet, $amount, $this);
                if ($transaction !== null) {
                    $mainWallet->addAmountWithoutAccrueToPartner($amount);
                    //return true;
                }

//
//                ExchangeUserOperations::dispatch($this->user);
//                event(new ExchangeAllOperations($amount));

            });
        } catch (\Illuminate\Database\QueryException $e) {
            // do what you want here with $e->getMessage();
        }

        return false;
    }

    /**
     * @param Wallet $wallet
     * @param Wallet $main_wallet
     * @param float $amount
     * @param $acc_rate
     * @return ExchangeOrder|bool
     * @throws \Exception
     */

    public function exchange(Wallet $wallet, Wallet $main_wallet, float $amount)
    {

        if ($amount <= 0) {
            return false;
        }


        $order = $this;
        $type = $order->type;
        $rate = $order->rate;
        $user = $wallet->user;

        if ($amount > $order->amount) {
            $amount = $order->amount;
        }

        $order->amount = $order->amount - $amount;


        if ($order->amount <= 0) {
            $order->active = 0;
        }

        $order->save();


        $rate_amount = $amount * $rate;

        $order_wallet = $order->mainWallet;
        $user_wallet = $wallet;
        $order_amount = $amount;
        $user_amount = $rate_amount;


        if ($type == ExchangeOrder::TYPE_SELL) {
            $order_wallet = $order->wallet;
            $user_wallet = $main_wallet;
            $order_amount = $rate_amount;
            $user_amount = $amount;
        }


        $fee_percent = (float)Setting::getValue('exchange_commission');

        $order_fee = $order_amount * $fee_percent * 0.01;
        $user_fee = $user_amount * $fee_percent * 0.01;


        $orderPieceData = [
            'order_id' => $order->id,
            'user_id' => $order->user->id,
            'main_currency_id' => $order->main_currency_id,
            'currency_id' => $order->currency_id,
            'main_wallet_id' => $order->main_wallet_id,
            'wallet_id' => $order->wallet_id,
            'rate' => $rate,
            'amount' => $amount,
            'type' => $type,
            'fee' => $order_fee,
            'rate_amount' => $amount * $rate,
            'limit_amount' => $amount * $rate * rate($wallet->currency->code, 'USD')
        ];

        /** @var OrderPiece $ownerPiece */
        $orderPiece = OrderPiece::create($orderPieceData);

        if ($orderPiece !== null) {
            $order_wallet->refresh();

            $order_wallet->balance +=($order_amount - $order_fee);
            $order_wallet->save();


            NotificationEvent::dispatch($order->user, 'notifications.' . $type == ExchangeOrder::TYPE_SELL ? 'sale' : 'purchase', [
                'user_id' => $order->user_id,
                'amount' => $order_amount,
                'currency' => $order->mainCurrency->code
            ]);

        }


        $userPieceData = [
            'order_id' => $order->id,
            'user_id' => $user->id,
            'main_currency_id' => $main_wallet->currency_id,
            'currency_id' => $wallet->currency_id,
            'main_wallet_id' => $main_wallet->id,
            'wallet_id' => $wallet->id,
            'rate' => $rate,
            'amount' => $amount,
            'type' => $type == ExchangeOrder::TYPE_SELL ? ExchangeOrder::TYPE_BUY : ExchangeOrder::TYPE_SELL,
            'fee' => $user_fee,
            'rate_amount' => $amount * $rate,
            'limit_amount' => $amount * $rate * rate($wallet->currency->code, 'USD')
        ];

        /** @var OrderPiece $ownerPiece */
        $userPiece = OrderPiece::create($userPieceData);

        if ($userPiece !== null) {
            $user_wallet->refresh();

            $user_wallet->balance +=($user_amount - $user_fee);
            $user_wallet->save();



//            if ($main_wallet->currency->code == 'FST') {
//                $limit_type = 'removeLimitsSell';
//
//                if ($type == ExchangeOrder::TYPE_SELL) {
//                    $limit_type = 'removeLimitsBuy';
//                }
//
//                $user->{$limit_type}($rate_amount, $wallet->currency->code);
//            }

            NotificationEvent::dispatch($user, 'notifications.' . $type == ExchangeOrder::TYPE_BUY ? 'sale' : 'purchase', [
                'user_id' => $user->id,
                'amount' => $user_amount,
                'currency' => $order->mainCurrency->code
            ]);

        }

//
       // event(new ExchangeAllOperations($amount));
        return $order;
    }


    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }



    /**
     * @param $query
     * @param $type
     * @return mixed
     */
    public function scopeOfType($query, $type)
    {
        return $query->where('type', $type);
    }


    public function scopeBetween($query, Carbon $from, Carbon $to)
    {
        $query->whereBetween('exchange_orders.created_at', [$from, $to]);
    }


}
